# Web Messenger in node.js #

This project demonstrates simple chat application by using node.js and socket.io. 

### Here are basic functions of this app###

* Single chat room for every users.
* Chat window will be displayed as a web page.
* Users have to input username before entering chat room.
* Users are able to send text and images into chat room
* For text input, users are able to use basic markdown for decorating the text. For example: \*\*asterisk\*\* will produced the bold text **asterisk** (<b>asterisk</b>)
* Chat history is saved for the latest 100 messages. Every users can see the chat history, when they enter the chat room.


### Set up environment ###

* Make sure node.js is installed
* Open the console and navigate to project folder and execute
```
npm install
```
* Once the download is complete, execute the command to start the app
```
node app.js
```
* By default, the listening port is 5000. You can use your browser to access the app at http://localhost:5000/