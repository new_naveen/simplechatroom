$(document).ready(function () {
    var ready = false;
    var socket = io();
    var currentUser = "";
    $("#chat-input-form").hide();
    $("#username-textbox").focus();

    $("form").submit(function (event) {
        event.preventDefault();
    });

    $("#join-button").click(function () {
        var username = $("#username-textbox").val();
        if (username != "") {

            socket.emit("join", username);

        }
        else{
            $("#join-error-message").text("Name is empty.");
        }
    });
    socket.on("join",function(message,name){
        if(message=="OK"){
            currentUser = name
            $("#login-form").detach();
            $("#chat-input-form").show();
            $("#chat-input-textbox").focus();
            ready = true;
        }
        else if(message=="REDUNDANT"){
            $("#join-error-message").text(name +" is online. Choose a different name");
        }

    });
    socket.on("loadChatHistory", function (chatHistory) {
        if (ready) {
            var messageClassName = 'chat-message';
            for (var i = 0; i < chatHistory.length; i++) {
                var name = chatHistory[i].name;
                var messageClassName = 'chat-message';
                if (currentUser == name) {
                    messageClassName += '-self';
                }
                if (chatHistory[i].type == "text") {
                    var markdownConverter = new showdown.Converter();
                    var html = markdownConverter.makeHtml(name + ": " + chatHistory[i].message);
                    $("#chat-history").append("<li class='" + messageClassName + "'>" + html + "</li>");
                }
                else if (chatHistory[i].type == "image") {
                    $("#chat-history").append("<li class='" + messageClassName + "'><p>" + name + " sent an image</p> <img class='chat-image' src=" + chatHistory[i].message + "></li>");
                }
            }
            $("#chat-history").scrollTop($("#chat-history").prop('scrollHeight'));
        }
    })
    socket.on("removeOldestChat", function () {
        if (ready) {
            $("#chat-history li.chat-message").first().remove();
        }
    })
    socket.on("update", function (msg) {
        if (ready)
            $("#chat-history").append("<li class='update-message'>" + msg + "</li>");
            $("#chat-history").scrollTop($("#chat-history").prop('scrollHeight'));
    })

    socket.on("update-user", function (allUsers) {
        if (ready) {
            $("#users").empty();
            $('#users').append("Users online: ");
            $.each(allUsers, function (clientid, name) {
                $('#users').append(name + " ");
            });
        }
    });

    socket.on("send-message", function (name, message) {
        if (ready) {
            var markdownConverter = new showdown.Converter();
            var html = markdownConverter.makeHtml(name + ": " + message);
            var messageClassName = 'chat-message';
            if (currentUser == name) {
                messageClassName += '-self';
            }
            $("#chat-history").append("<li class='" + messageClassName + "'>" + html + "</li>");
            $("#chat-history").scrollTop($("#chat-history").prop('scrollHeight'));
        }
    });
    socket.on('send-file', function (name, file) {
         var messageClassName = 'chat-message';
            if (currentUser == name) {
                messageClassName += '-self';
            }
        $("#chat-history").append("<li class='"+messageClassName+"'><p>" + name + " sent an image</p> <img class='chat-image' src=" + file + "></li>");
        $("#chat-history").scrollTop($("#chat-history").prop('scrollHeight'));
    });
    socket.on("disconnect", function () {
        $("#chat-history").append("The server is not available");
        $("#chat-input-textbox").attr("disabled", "disabled");
        $("#send-button").attr("disabled", "disabled");
    });


    $("#send-button").click(function () {
        var msg = $("#chat-input-textbox").val();
        if (msg != "") {
            socket.emit("send-message", msg);
            $("#chat-input-textbox").val("");
        }
    });

    $('#upload-image-button').bind('change', function (e) {
        var data = e.originalEvent.target.files[0];
        var reader = new FileReader();
        reader.onload = function (evt) {
            var msg = {};
            msg.username = currentUser;
            msg.file = evt.target.result;
            msg.fileName = data.name;
            socket.emit('send-file', msg);
            $('#upload-image-button').val("");
        };
        reader.readAsDataURL(data);
    });
});