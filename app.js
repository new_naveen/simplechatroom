var express = require('express');
var socketIO = require('socket.io')
var routes = require('./routes/index');

var app = express()
app.use('/', routes);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.static('public'));

var PORT = 5000;
var MAX_NUMBER_CHAT = 100;

var server = app.listen(PORT, function () {
    console.log('port: ' + PORT);
});

var serverSocket = socketIO.listen(server);
var allUsers = {};
var allNames={};
var chatHistory = [];
serverSocket.on("connection", function (client) {
    client.on("join", function (name) {
        if(name in allNames){
            client.emit("join", "REDUNDANT",name)
        }
        else {
            client.emit("join", "OK",name)
            allUsers[client.id] = name;
            allNames[name]=true;
            client.emit("loadChatHistory", chatHistory);
            serverSocket.sockets.emit("update", name + " joined the chat.");
            serverSocket.sockets.emit("update-user", allUsers);
        }
    });

    client.on("send-message", function (message) {
        addToChatHistory(allUsers[client.id], message, "text")
        serverSocket.sockets.emit("send-message", allUsers[client.id], message);
    });
    client.on('send-file', function (message) {
        addToChatHistory(allUsers[client.id], message.file, "image")
        serverSocket.sockets.emit('send-file', allUsers[client.id], message.file);
        //message.fileName

    });

    client.on("disconnect", function () {
        serverSocket.sockets.emit("update", allUsers[client.id] + " left the chat.");
        var name = allUsers[client.id]
        delete allUsers[client.id];
        delete allNames[name];
        serverSocket.sockets.emit("update-user", allUsers);
    });
});

function addToChatHistory(name, message, type) {
    if (chatHistory.length >= MAX_NUMBER_CHAT) {
        chatHistory.splice(0, 1);
        serverSocket.sockets.emit("removeOldestChat")
    }
    var chatObj = {
        "name": name,
        'message': message,
        'type': type
    }
    chatHistory.push(chatObj);
}

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
